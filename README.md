# PDEngine.PDChat-CPP

## Name

PDChat Client-only implementation in C++ with the boost::asio and the CryptoPP library
Core implementation of [PDChat](https://gitlab.com/Rawra/pdengine.pdchat)

## Installation

Clone and build the solution using the Visual Studio sln for RELEASE (VS2022 v143 / C++20)

## Usage

Connect to any PDUDPSock Chat server via IPv4/IPv6 and start typing.
Valid arguments are: app.exe <server_listen_port> <client_listen_port>

Defaults are:
client_listen_port: 7777
server_listen_port: 7778

## Dependencies

This library requires boost::asio 1.84.0, CryptoPP and msgpack-cxx to work

The recommended way to install the dependencies is to use vcpkg to install cryptopp, then download
boost::asio and msgpack-cxx off their origin sites.

```
vcpkg install cryptopp --triplet=x64-windows
```

Be sure to check the sln include/lib paths and make them match with your wished directories.

| Library     | Link                                                    |
|-------------|---------------------------------------------------------|
| boost::asio | https://www.boost.org/users/history/version_1_84_0.html |
| msgpack-cxx | https://github.com/msgpack/msgpack-c                    |
| CryptoPP    | https://cryptopp.com/                                   |

## Screenshots

![ALT](/Screenshots/Screenshot1.png)
![ALT](/Screenshots/Screenshot2.png)

## Roadmap

This project is considered feature complete and will only be updated to keep up with the changes of the pdchat main C# branch.