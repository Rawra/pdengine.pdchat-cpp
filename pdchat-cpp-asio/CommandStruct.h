#pragma once

enum CommandIdentifier {
	Initiate = 0,
	List = 1,
	Identify = 2
};
MSGPACK_ADD_ENUM(CommandIdentifier)

struct CommandStruct_t {
	CommandIdentifier commandIdentifier;
	std::vector<std::string> a_string_arr;
	std::vector<uint8_t> a_bytes_one;
	std::vector<uint8_t> a_bytes_two;
	std::string a_string;
	bool a_bool;
	MSGPACK_DEFINE(commandIdentifier, a_string_arr, a_bytes_one, a_bytes_two, a_string, a_bool);
};