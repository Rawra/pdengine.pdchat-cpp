#pragma once

using namespace CryptoPP;

// Encrypt any given std::vector via CBC w/ PKCS7 padding
std::vector<uint8_t> EncryptAES_CBC_PKCS7(const std::vector<uint8_t>& plaintext, const CryptoPP::byte* key, size_t length, const CryptoPP::byte* iv) {
    std::string ciphertext;
    try {
        CBC_Mode<AES>::Encryption e;
        e.SetKeyWithIV(key, length, iv);

        // Encryption
        StringSource s(std::string(plaintext.begin(), plaintext.end()), true,
            new StreamTransformationFilter(e,
                new StringSink(ciphertext)
            ) // StreamTransformationFilter
        ); // StringSource
    }
    catch (const CryptoPP::Exception& e) {
        std::cerr << "Error during encryption: " << e.what() << std::endl;
    }

    return std::vector<uint8_t>(ciphertext.begin(), ciphertext.end());
}

std::string EncryptAES_CBC_PKCS7_String(const std::string& plaintext, const CryptoPP::byte* key, size_t length, const CryptoPP::byte* iv) {
    std::string ciphertext;
    try {
        CBC_Mode<AES>::Encryption e;
        //e.SetKeyWithIV(aes_key.data(), aes_key.size(), aes_iv.data());
        //e.SetKeyWithIV(aes_key.data(), AES::DEFAULT_KEYLENGTH, aes_iv.data(), AES::BLOCKSIZE);
        e.SetKeyWithIV(key, AES::DEFAULT_KEYLENGTH, iv, AES::BLOCKSIZE);

        // Encryption
        StringSource s(plaintext, true,
            new StreamTransformationFilter(e,
                new StringSink(ciphertext)
            ) // StreamTransformationFilter
        ); // StringSource
    }
    catch (const CryptoPP::Exception& e) {
        std::cerr << "Error during encryption: " << e.what() << std::endl;
    }

    return ciphertext;
}

// Decrypt any given std::vector via CBC w/ PKCS7 padding
std::vector<uint8_t> DecryptAES_CBC_PKCS7(const std::vector<uint8_t>& ciphertext, const CryptoPP::byte* key, size_t length, const CryptoPP::byte* iv) {
    std::string decryptedtext;
    try {
        CBC_Mode< AES >::Decryption d;
        d.SetKeyWithIV(key, length, iv);

        StringSource s(std::string(ciphertext.begin(), ciphertext.end()), true,
            new StreamTransformationFilter(d,
                new StringSink(decryptedtext)
            ) // StreamTransformationFilter
        ); // StringSource
    }
    catch (const CryptoPP::Exception& e) {
        std::cerr << "Error during decryption: " << e.what() << std::endl;
    }

    return std::vector<uint8_t>(decryptedtext.begin(), decryptedtext.end());
}

std::string DecryptAES_CBC_PKCS7_String(const std::string& ciphertext, const CryptoPP::byte* key, size_t length, const CryptoPP::byte* iv) {
    //std::vector<uint8_t> decryptedtext;
    std::string decryptedtext;
    try {
        CBC_Mode< AES >::Decryption d;
        d.SetKeyWithIV(key, length, iv);

        StringSource s(ciphertext, true,
            new StreamTransformationFilter(d,
                new StringSink(decryptedtext)
            ) // StreamTransformationFilter
        ); // StringSource
    }
    catch (const CryptoPP::Exception& e) {
        std::cerr << "Error during decryption: " << e.what() << std::endl;
    }

    return decryptedtext;
}