#pragma once

// Boost lib
#include <boost/asio/awaitable.hpp>
#include <boost/asio/detached.hpp>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/io_context.hpp>
//#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/redirect_error.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/use_awaitable.hpp>
#include <boost/asio/write.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string/case_conv.hpp>

// CryptoPP lib
#include <cryptopp/aes.h>
#include <cryptopp/modes.h>
#include <cryptopp/filters.h>
#include <cryptopp/hex.h>
#include <cryptopp/files.h>
#include <cryptopp/rijndael.h>
#include <cryptopp/osrng.h>
#include <cryptopp/cryptlib.h>

// Msgpack
#include <msgpack.hpp>

// Custom
#include "AESUtil.h"
#include "CommandStruct.h"
#include "PDNetworkObject.h"
#include "PDNetworking.h"

// Directives
//#define _DEBUG_PRINT


using boost::asio::ip::udp;
//using boost::asio::awaitable;
//using boost::asio::co_spawn;
//using boost::asio::detached;
using boost::asio::redirect_error;
//using boost::asio::use_awaitable;
using boost::asio::ip::address;
using boost::asio::buffer;
using boost::asio::dynamic_buffer;

// global buffer size for packets
constexpr uint32_t DEF_BUFFER_SIZE = 1024 * 64;

struct PDUDPSock {
    PDUDPSock(
        boost::asio::ip::address local_end_point_addr, uint16_t local_end_point_port, 
        boost::asio::ip::address remote_end_point_addr, uint16_t remote_end_point_port,
        bool enable_aes = false) 
    {
        this->local_end_point = udp::endpoint(local_end_point_addr, local_end_point_port);
        this->client_remote_endpoint = udp::endpoint(remote_end_point_addr, remote_end_point_port);
        this->aes_enabled = enable_aes;

        switch (local_end_point.data()->sa_family) {
        case AF_INET:
            socket.open(udp::v4());
            break;
        case AF_INET6:
            socket.open(udp::v6());
            break;
        }
        socket.bind(local_end_point);

        thread_heartbeat = std::thread([&] { client_heartbeat_send(); });
        thread_heartbeat.detach();
    }
    // our local socket where the data we send comes from
    udp::endpoint local_end_point;

    // the server endpoint where we send data to
    udp::endpoint client_remote_endpoint;

    // Settings
    uint64_t def_heartbeat_query_ms = 5000;
    uint64_t def_heartbeat_send_ms = 3000;
    std::atomic<bool> heartbeat_enabled = true;

    // Threads
    std::thread thread_heartbeat;
    std::thread thread_listen;
    std::atomic<bool> thread_kill_enabled = false;

    // boost::asio data
    boost::asio::io_context io_context;
    udp::socket socket{ io_context };
    boost::array<char, DEF_BUFFER_SIZE> recv_buffer;

    // network data
    udp::endpoint remote_endpoint; // will be set as soon we get inbound data

    // AES-Section
    std::vector<uint8_t> aes_key;
    std::vector<uint8_t> aes_iv;
    std::atomic<bool> aes_initiated = false;
    bool aes_enabled;

    // handle the incoming server message
    void client_handle_incoming(const boost::system::error_code& error, size_t bytes_transferred) {
        if (error) {
            std::cout << "Receive failed: " << error.message() << "\n";
            return;
        }

        // Check size of incoming
        if (bytes_transferred > DEF_BUFFER_SIZE) {
            printf_s("Incoming packet was too huge for internal buffer. Dropping...\n");
            return;
        }

        // Parse the pdno
        PDNetworkObject_t pdno;

        // Unpack the msgpack
        // Default construct msgpack::object_handle
        msgpack::object_handle result;
        // Pass the msgpack::object_handle
        msgpack::unpack(result, recv_buffer.data(), bytes_transferred);
        // Get msgpack::object from msgpack::object_handle (shallow copy)
        msgpack::object obj(result.get());
        pdno = obj.convert();

        // Handle the status type
        std::string text;
        std::vector<uint8_t> decryptedData;
        switch (pdno.status_type) {

        // Default server msg
        case PDNetworkObjectStatus::Information:

            if (aes_enabled) {
                decryptedData = DecryptAES_CBC_PKCS7(pdno.value, aes_key.data(), aes_key.size(), aes_iv.data());
                text = std::string(decryptedData.begin(), decryptedData.end());
                //text.resize(pdno.size);
                printf_s("%s\n", text.c_str());
                break;
            }
            text = std::string(pdno.value.begin(), pdno.value.end());
            text.resize(pdno.size);
            printf_s("%s\n", text.c_str());
            break;

            // Server command reply
        case PDNetworkObjectStatus::TestReply:
            client_handle_reply(pdno);
            break;
        }

        client_listen_recvfrom();
    }

    // Handle all server special replies (commandstructs, etc)
    void client_handle_reply(const PDNetworkObject_t& pdno) {
        // Parse the embedded CommandStruct
        CommandStruct_t cmd;

        // Unpack the msgpack
        msgpack::object_handle result;
        msgpack::unpack(result, (char*)pdno.value.data(), pdno.value.size());
        msgpack::object obj(result.get());
        cmd = obj.convert();

        HexEncoder encoder(new FileSink(std::cout));
        switch (cmd.commandIdentifier) {

        // AES Initiation from server
        case CommandIdentifier::Initiate:
            aes_key = cmd.a_bytes_one;
            aes_iv = cmd.a_bytes_two;
            aes_initiated = true;

#ifdef _DEBUG_PRINT
            printf_s("Received AES Initiation\n");

            std::cout << "Received-IV: ";
            encoder.Put((const byte*)&aes_iv[0], aes_iv.size());
            encoder.MessageEnd();
            std::cout << std::endl;

            std::cout << "Received-Key: ";
            encoder.Put((const byte*)&aes_key[0], aes_key.size());
            encoder.MessageEnd();
            std::cout << std::endl;
#endif
            break;

        // Identification reply
        case CommandIdentifier::Identify:
            if (cmd.a_bool) {
                printf_s("Admin privileges: GRANTED\n");
            }
            else {
                printf_s("Admin privileges: DENIED\n");
            }
            break;

        // List (active clients) reply
        case CommandIdentifier::List:
            printf_s("Users in the server: %lli\n", cmd.a_string_arr.size());
            for (std::string user : cmd.a_string_arr) {
                printf_s("%s\n", user.c_str());
            }
            break;
        }

    }

    // Start a local listener thread to receive packets from the server
    void client_listen()
    {
        thread_listen = std::thread([&] { client_listen_recv(); });
        thread_listen.detach();
    }

    // (Listen-Thread) Begin listening for packets
    void client_listen_recv() {
        client_listen_recvfrom();

        // Process all async IO events of asio (blocking)
        io_context.run();
        printf_s("Receiver (io_context) exit\n");
    }

    // (Listen-Thread) Receive From wrapper
    void client_listen_recvfrom() {
        // Check if we need to stop
        if (thread_kill_enabled) {
            return;
        }

        socket.async_receive_from(boost::asio::buffer(recv_buffer), remote_endpoint, boost::bind(&PDUDPSock::client_handle_incoming, this, _1, _2));
    }

    // Connect to a PDUDPSock server
    void client_connect(std::string username) {
        // Create connect pdno struct
        PDNetworkObject_t pdno = create_connect_packet(username);

        // create buffer
        msgpack::sbuffer sb;
        msgpack::pack(sb, pdno);

        boost::system::error_code err;
        auto sent = socket.send_to(boost::asio::buffer(sb.data(), sb.size()), client_remote_endpoint, 0, err);
        if (err) {
            printf_s("connect error: %s\n", err.message().c_str());
        }
    }

    // Disconnect from a PDUDPSock server
    void client_disconnect() {
        // Kill threads
        thread_kill_enabled = true;

        // Create connect pdno struct
        PDNetworkObject_t pdno = create_disconnect_packet();

        // create buffer
        msgpack::sbuffer sb;
        msgpack::pack(sb, pdno);

        boost::system::error_code err;
        auto sent = socket.send_to(boost::asio::buffer(sb.data(), sb.size()), client_remote_endpoint, 0, err);
        if (err) {
            printf_s("disconnect error: %s\n", err.message().c_str());
        }
    }

    // Send data to server from client
    void client_send(std::string in) {
        // Create connect pdno struct
        PDNetworkObject_t pdno;
        if (aes_enabled) {
            std::string encryptedtext = EncryptAES_CBC_PKCS7_String(in, aes_key.data(), aes_key.size(), aes_iv.data());
            pdno = create_info_packet(encryptedtext);

#ifdef _DEBUG_PRINT
            // Print aes stuff (debug only)
            HexEncoder encoder(new FileSink(std::cout));
            std::cout << "cipher text: ";
            encoder.Put((const byte*)&encryptedtext[0], encryptedtext.size());
            encoder.MessageEnd();
            std::cout << std::endl;

            std::cout << "Used-IV: ";
            encoder.Put((const byte*)&aes_iv[0], aes_iv.size());
            encoder.MessageEnd();
            std::cout << std::endl;

            std::cout << "Used-Key: ";
            encoder.Put((const byte*)&aes_key[0], aes_key.size());
            encoder.MessageEnd();
            std::cout << std::endl;
#endif
        }
        else {
            pdno = create_info_packet(in);
        }

        // create buffer
        msgpack::sbuffer sb;
        msgpack::pack(sb, pdno);

        boost::system::error_code err;
        auto sent = socket.send_to(boost::asio::buffer(sb.data(), sb.size()), client_remote_endpoint, 0, err);
        if (err) {
            printf_s("send error: %s\n", err.message().c_str());
        }
    }

    // (Heartbeat-Thread) Send heartbeat to server
    void client_heartbeat_send() {
        // Check if we need to stop
        if (thread_kill_enabled) {
            return;
        }

        // Check if we need to send heartbeats currently
        if (!heartbeat_enabled) {
            std::this_thread::sleep_for(std::chrono::milliseconds(def_heartbeat_send_ms));
            client_heartbeat_send();
        }

        // Create heartbeat pdno struct
        PDNetworkObject_t pdno = create_heartbeat_packet();

        // Create buffer
        msgpack::sbuffer sb;
        msgpack::pack(sb, pdno);

        boost::system::error_code err;
        auto sent = socket.send_to(boost::asio::buffer(sb.data(), sb.size()), client_remote_endpoint, 0, err);
        if (err) {
            printf_s("heartbeat error: %s\n", err.message().c_str());
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(def_heartbeat_send_ms));
        client_heartbeat_send();
    }
};