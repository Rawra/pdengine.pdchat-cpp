#pragma once


PDNetworkObject_t create_info_packet(const std::string& username) {
    //std::cout << "create_info_packet" << std::endl;
    PDNetworkObject_t pdno;
    pdno.status_type = PDNetworkObjectStatus::Information;
    pdno.object_type = PDNetworkObjectType::String;
    pdno.version = 3;
    pdno.value = std::vector<uint8_t>(username.begin(), username.end());
    pdno.size = (0x01 * 3) + pdno.value.size() + 0x04;
    return pdno;
}

PDNetworkObject_t create_heartbeat_packet() {
    //std::cout << "create_heartbeat_packet" << std::endl;
    PDNetworkObject_t pdno;
    pdno.status_type = PDNetworkObjectStatus::Heartbeat;
    pdno.object_type = PDNetworkObjectType::Bytes;
    pdno.version = 3;
    std::vector<uint8_t> v{ 0x03 };
    pdno.value = v;
    pdno.size = (0x01 * 3) + pdno.value.size() + 0x04;
    return pdno;
}

PDNetworkObject_t create_connect_packet(const std::string& username) {
    //std::cout << "create_connect_packet" << std::endl;
    PDNetworkObject_t pdno;
    pdno.status_type = PDNetworkObjectStatus::Connect;
    pdno.object_type = PDNetworkObjectType::String;
    pdno.version = 3;
    pdno.value = std::vector<uint8_t>(username.begin(), username.end());
    pdno.size = (0x01 * 3) + pdno.value.size() + 0x04;
    return pdno;
}

PDNetworkObject_t create_disconnect_packet() {
    //std::cout << "create_disconnect_packet" << std::endl;
    PDNetworkObject_t pdno;
    pdno.status_type = PDNetworkObjectStatus::Disconnect;
    pdno.object_type = PDNetworkObjectType::Bytes;
    pdno.version = 3;
    std::vector<uint8_t> v{ 0x00 };
    pdno.value = v;
    pdno.size = (0x01 * 3) + pdno.value.size() + 0x04;
    return pdno;
}