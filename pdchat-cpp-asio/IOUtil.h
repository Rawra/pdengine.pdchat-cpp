#pragma once

/// <summary>
/// Try to get the port of a socketstring
/// </summary>
/// <param name="str"></param>
/// <returns></returns>
std::optional<uint16_t> get_port_by_socketstring(const std::string& str) {
    size_t pos = str.find(":");
    size_t len = str.length();
    if (pos > len) return std::nullopt;
    std::string ss = str.substr(pos + 1, len - pos);
    return std::make_optional(std::atoi(ss.c_str()));
}

std::optional<std::string> get_address_by_socketstring(const std::string& str) {
    size_t pos = str.find(":");
    size_t len = str.length();
    if (pos > len) return std::nullopt;
    std::string ss = str.substr(0, pos);
    return std::make_optional(ss.c_str());
}

/// <summary>
/// Try to get a valid IP Address from the user
/// </summary>
/// <returns></returns>
boost::asio::ip::address get_server_ip() {
    std::string strAddress;
    boost::asio::ip::address finalAddress;

    printf_s("Please type in a IPv4/IPv6 address: ");
    while (true) {
        std::cin >> strAddress;
        std::cin.ignore(); // Ignore \n

        boost::system::error_code ec;
        finalAddress = boost::asio::ip::make_address(strAddress, ec);
        if (ec) {
            printf_s("%s\n", ec.message().c_str());
            continue;
        }
        return finalAddress;
    }
}

/// <summary>
/// Try to get a valid nickname from the user
/// </summary>
/// <returns></returns>
std::string get_nickname_by_user() {
    std::string nickname;

    printf_s("Please type in a nickname.: ");
    while (true) {
        std::getline(std::cin, nickname);

        if (nickname.length() <= 0) {
            printf_s("Please enter a valid nickname\n");
            continue;
        }
        return nickname;
    }
}