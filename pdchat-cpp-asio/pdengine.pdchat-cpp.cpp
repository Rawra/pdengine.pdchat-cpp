#include <cstdlib>
#include <deque>
#include <iostream>
#include <list>
#include <memory>
#include <set>
#include <string>
#include <utility>
#include <atomic>
#include <mutex>

// Msgpack
#include <msgpack.hpp>

// Custom
#include "PDUDPSock.hpp"
#include "IOUtil.h"

int main(int argc, char* argv[]) {
    // Parse commandline (pdchat-cpp.exe serverlistenport clientlistenport)
    uint16_t server_listen_port = 7777;
    uint16_t client_listen_port = 0; // Default: 7778
    if (argc > 1 && argc < 3) {
        server_listen_port = std::atoi(argv[2]);
        client_listen_port = std::atoi(argv[3]);
    }

    // Proper start
    printf_s("PDEngine.Chat-cpp: Loaded\n");
    printf_s("Version: 1.0.0\n");
    printf_s("Server Listen Port: %u\n", server_listen_port);
    printf_s("Client Listen Port: %u\n\n", client_listen_port);

    // Let the user give us a IP Address
    boost::asio::ip::address server_ip = get_server_ip();
    printf_s("Selected Server IP: %s\n\n", server_ip.to_string().c_str());

    // Let the user give us a nickname
    std::string username = get_nickname_by_user();

    // Setup the local listener
    PDUDPSock pdsock(boost::asio::ip::make_address("0.0.0.0"), client_listen_port, server_ip, server_listen_port, true);
    std::this_thread::sleep_for(std::chrono::milliseconds(200)); // Wait just a bit before sending stuff over

    // Formally connect
    pdsock.client_connect(username);
    pdsock.client_listen();

    // Keep alive until /quit
    std::string input;
    printf_s("You may now begin chatting! Have fun!\n");
    while (true) 
    {
        input.clear();
        std::getline(std::cin, input);

        if (input.compare("/quit") == 0) {
            pdsock.client_disconnect();
            printf_s("Quitting...\n");
            break;
        }
        pdsock.client_send(input);
    }
}