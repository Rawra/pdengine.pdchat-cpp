#pragma once

enum class PDNetworkObjectStatus : uint32_t {
	Information = 0,
	Connect = 1,
	Disconnect = 2,
	Heartbeat = 3,
	TestQuery = 4,
	TestReply = 5
};
MSGPACK_ADD_ENUM(PDNetworkObjectStatus)

enum class PDNetworkObjectType : uint32_t {
	String = 0,
	Int16 = 1,
	Int32 = 2,
	Int64 = 3,
	UInt16 = 4,
	UInt32 = 5,
	UInt64 = 6,
	Bool = 7,
	Float = 8,
	Double = 9,
	Bytes = 10
};
MSGPACK_ADD_ENUM(PDNetworkObjectType)

struct PDNetworkObject_t {
	PDNetworkObjectStatus status_type;
	PDNetworkObjectType object_type;
	uint8_t version;
	std::vector<uint8_t> value;
	int64_t size;
	MSGPACK_DEFINE(status_type, object_type, version, value, size);
};